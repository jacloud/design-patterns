
public interface ProductFactory {
	Product buy(int amount);
}
