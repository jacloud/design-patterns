
public class ChocolateFactory implements ProductFactory{
	
	@Override
	public Product buy(int amount) {
		System.out.println("you expend " + amount * 1.5f + "�");
		return new Chocolate();
	}
}
