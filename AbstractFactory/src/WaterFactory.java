
public class WaterFactory implements ProductFactory{
	
	@Override
	public Product buy(int amount) {
		System.out.println("you expend " + amount * 0.5f + "�");
		return new Water();
	}
}
