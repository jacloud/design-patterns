import java.util.HashMap;
import java.util.Map;

public class Shop {

	Map<String, ProductFactory> products;
	
	public Shop() {
		products = new HashMap<>();
		products.put("Chocolate", new ChocolateFactory());
		products.put("Water", new WaterFactory());
	}
}
