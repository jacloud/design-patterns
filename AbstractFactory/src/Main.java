
public class Main {

	public static void main(String[] args) {
		Shop shop = new Shop();
		shop.products.get("Water").buy(6).consume();
		shop.products.get("Chocolate").buy(1).consume();
	}

}
