public class KingGoblin extends Goblin {
	
	private static final int ATTACK = 3;
	private static final int DEFENSE = 3;
	
	public KingGoblin() {
		super();
		attack = ATTACK;
		defense = DEFENSE;
		Game.addModifier(this, Game.Modifier.KING);
	}

}
