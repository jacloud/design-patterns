
abstract class Monster {
	
	public abstract int getAttack();
	public abstract int getDefense();
	
}
