
public class KingModifier {

	public KingModifier(Event<Query> queries, Monster monster) {
		
		queries.subscribe(q -> { if(q.monster != monster && q.argument == Query.Argument.ATTACK ) {
				q.result += 1;
			} } );
	}
}
