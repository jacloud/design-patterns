public class Goblin extends Monster {
	
	private static final int ATTACK = 1;
	private static final int DEFENSE = 1;
	protected int attack;
	protected int defense;
	
	
	public Goblin() {
		attack = ATTACK;
		defense = DEFENSE;
		Game.addModifier(this, Game.Modifier.GROUP);
	}

	@Override
	public int getAttack() {
		return Game.checkAttack(this, attack);
	}

	@Override
	public int getDefense() {
		return Game.checkDefense(this, defense);
	}
	
	@Override
	public String toString() {
		return "Goblin with attack: " + getAttack() + " and defense: " + getDefense();
	}
}
