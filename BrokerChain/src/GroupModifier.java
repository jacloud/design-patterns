
public class GroupModifier {

	public GroupModifier(Event<Query> queries, Monster monster) {
		super();
		
		queries.subscribe(q -> { if( q.monster != monster && q.argument == Query.Argument.DEFENSE ) {
			q.result += 1;
		} } );
	}

}
