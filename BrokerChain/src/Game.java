class Game {
	
	private static final Game INSTANCE = new Game();
	
	public enum Modifier
	  {
	    GROUP, KING
	  }
	
	private Game() {
		
	}
	
	public static Game getInstance() {
		return INSTANCE;
	}
	
	private static Event<Query> queries = new Event<>();;
	
	public static Goblin addGoblin() {
		return new Goblin();
	}
	
	public static KingGoblin addKingGoblin() {
		return new KingGoblin();
	}
	
	public static int checkAttack(Monster m, int baseAttack) {
		Query q = new Query(m, Query.Argument.ATTACK, baseAttack);
	    queries.fire(q);
	    return q.result;
	}
	
	public static int checkDefense(Monster m, int baseDefense) {
		Query q = new Query(m, Query.Argument.DEFENSE, baseDefense);
	    queries.fire(q);
	    return q.result;
	}
	
	public static void addModifier(Monster monster, Modifier modifier) {
		switch (modifier) {
			case GROUP:
				new GroupModifier(queries, monster);
				break;
			case KING:
				new KingModifier(queries, monster);
				break;
			default:
				break;
		}
	}
	
}
