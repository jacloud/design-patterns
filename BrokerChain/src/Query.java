
public class Query {

	  enum Argument
	  {
	    ATTACK, DEFENSE
	  }
	  
	  public Argument argument;
	  public int result;
	  public Monster monster;

	  public Query(Monster monster, Argument argument, int result)
	  {
	    this.argument = argument;
	    this.result = result;
	    this.monster = monster;
	  }
}
