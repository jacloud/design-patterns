package htmlBuilder;

import java.util.ArrayList;
import java.util.Collections;

public class HtmlElement {
	
	private static final int INDENT_SIZE = 2;
	private static final String NEW_LINE = System.lineSeparator();
	private static final String SPACE = " ";
	
	
	private String name, text;
	private ArrayList<HtmlElement> elements;
	
	public HtmlElement() {
		elements = new ArrayList<>();
	}
	
	public HtmlElement(String name) {
		this(name, null);
	}
	
	public HtmlElement(String name, String text) {
		this();
		this.name = name;
		this.text = text;
	}
	
	public HtmlElement addChild(String name, String text) {
		HtmlElement e = new HtmlElement(name, text);
		elements.add(e);
		return e;
	}
	
	private String toStringImpl(int indent) {
		StringBuilder sb = new StringBuilder();
		String indentString = String.join("", Collections.nCopies(indent * INDENT_SIZE, SPACE));
		sb.append(String.format("%s<%s>%s",  indentString, name, NEW_LINE));
		if(text != null && !text.isEmpty()) {
			sb.append(String.join("", Collections.nCopies(indent * INDENT_SIZE + 1, SPACE))).append(text).append(NEW_LINE);
		}
		
		for(HtmlElement e : elements) {
			sb.append(e.toStringImpl(indent + 1));
		}
		
		sb.append(String.format("%s</%s>%s",  indentString, name, NEW_LINE));
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return toStringImpl(0);
	}
	
}
