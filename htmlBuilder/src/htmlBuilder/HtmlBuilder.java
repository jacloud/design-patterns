package htmlBuilder;

public class HtmlBuilder {
	
	private String rootName;
	private HtmlElement root;
	
	public HtmlBuilder(String rootName) {
		this.rootName = rootName;
		root = new HtmlElement(rootName);
	}
	
	public HtmlElement addChild(String childName, String childText) {
		return root.addChild(childName, childText);
	}
	
	public void clear() {
		root = new HtmlElement(rootName);
	}
	
	@Override
	public String toString() {
		return root.toString();
	}
}
