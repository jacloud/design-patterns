
public class RasterSquare implements Render
{	
	@Override
	public String render(String name)
	{
		return String.format("Drawing %s as pixels", name);
	}
}
