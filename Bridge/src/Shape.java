
public abstract class Shape {
	public Render render;
	
	public Shape(Render render) {
		this.render = render;
	}
	
	public abstract String getName();
}
