
public class Square extends Shape {
	
	public Square(Render render) {
		super(render);
	}
	
	@Override
	public String toString() {
		return render.render(getName());
	}
	
	@Override
	public String getName()
	{
	  return "Square";
 	}
  
}

