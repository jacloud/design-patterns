
public class Constants {

	public static final int INDENT_SIZE = 2;
	public static final String NEW_LINE = System.lineSeparator();
	public static final String SPACE = " ";
	public static final String PUBLIC = "public";
	public static final String CLASS = "class";
	public static final String OPEN_CLASS_DEFINITION = "{";
	public static final String CLOSE_CLASS_DEFINITION = "}";
	public static final String SEMICOLON = ";";
	
	private Constants() {
		
	}
}
