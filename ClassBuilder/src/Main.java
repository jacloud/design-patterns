
public class Main {
	public static void main(String[] args) {
		ClassBuilder classBuilder = new ClassBuilder("Person");
		classBuilder.attributes().addField("name", "String").addField("age", "int");
		System.out.println(classBuilder);
	}
}
