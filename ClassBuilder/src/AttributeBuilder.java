
public class AttributeBuilder extends ClassBuilder {
	
	public AttributeBuilder(String className) {
		super(className);
	}
	
	public AttributeBuilder(Class classBuild) {
		super(classBuild.getName());
		this.classBuild = classBuild;
	}
	
	public AttributeBuilder addField(String name, String type) {
		classBuild.addAttribute(name, type);
		return this;
	}
}
