import java.util.Collections;

public class Attribute {
	
	private String name;
	private String type;
	private StringBuilder sb;
	
	public Attribute(String name, String type) {
		this.name = name;
		this.type = type;
		sb = new StringBuilder();
	}
	
	protected String toStringImpl(int indent) {
		String indentString = String.join("", Collections.nCopies(indent * Constants.INDENT_SIZE, Constants.SPACE));
		sb.append(String.format("%s %s %s %s%s%s", indentString, Constants.PUBLIC, type, name, Constants.SEMICOLON, Constants.NEW_LINE));		
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return toStringImpl(0);
	}
}
