import java.util.ArrayList;
import java.util.List;

public class Class {
	
	private String name;
	private List<Attribute> attributeList;
	private StringBuilder sb;
	
	public Class() {
		attributeList = new ArrayList<>();
		sb = new StringBuilder();
	}
	
	public Class(String name) {
		this();
		this.name = name;
	}
	
	public void addAttribute(String name, String type) {
		attributeList.add(new Attribute(name, type));
	}
	
	private String toStringImpl(int indent) {
		sb.append(String.format("%s %s %s %s%s%s",  Constants.PUBLIC, Constants.CLASS, name, Constants.NEW_LINE, Constants.OPEN_CLASS_DEFINITION, Constants.NEW_LINE));		
		for(Attribute attribute : attributeList) {
			sb.append(attribute.toStringImpl(indent + 1));
		}
		sb.append(String.format("%s",  Constants.CLOSE_CLASS_DEFINITION));
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return toStringImpl(0);
	}
	
	public String getName() {
		return name;
	}
	
	
}
