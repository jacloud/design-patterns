
public class ClassBuilder {
	
	protected Class classBuild;
	
	public ClassBuilder(String className) {
		classBuild = new Class(className);
	}
	
	public Class build() {
		return classBuild;
	}
	
	public AttributeBuilder attributes() {
		return new AttributeBuilder(classBuild);
	}
	
	@Override
	public String toString() {
		return classBuild.toString();
	}
}
