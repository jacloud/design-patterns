import java.util.Collections;
import java.util.Iterator;

public class SingleValue implements ValueContainer {
	public int value;

	public SingleValue(int value) {
		this.value = value;
	}

	@Override
	public Iterator<Integer> iterator() {
		// TODO Auto-generated method stub
		return Collections.singleton(value).iterator();
	}
}
