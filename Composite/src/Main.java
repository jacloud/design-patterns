import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		SingleValue v1 = new SingleValue(5);
		ManyValues v2 = new ManyValues();
		v2.add(7);
		v2.add(9);
		
		ArrayList<ValueContainer> test = new ArrayList<>();
		test.add(v1);
		test.add(v2);
		
		MyList list = new MyList(test);
		
		System.out.println(list.sum());
	}

}
