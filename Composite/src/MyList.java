import java.util.ArrayList;
import java.util.Collection;

public class MyList extends ArrayList<ValueContainer> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -582098881005153225L;

	public MyList(Collection<? extends ValueContainer> c) {
	  super(c);
	}

	public int sum() {
		int res = 0;
	    for(ValueContainer value : this) {
	    	for(Integer num : value) {
	    		res += num;
	    	}
	    }
	    return res;
	}
}
