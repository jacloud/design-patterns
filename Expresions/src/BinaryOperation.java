
public class BinaryOperation implements Element {
	
	public static enum typeOperation {
		ADDTION,
		SUBTRACTION,
		NO_OPERATION
	}

	public typeOperation type = typeOperation.NO_OPERATION;
	public Element left, right;
	
	public boolean fail;
	
	@Override
	public int eval() {
		if(fail) return 0;
		switch (type) {
			case ADDTION:
				return left.eval() + right.eval();
			case SUBTRACTION:
				return left.eval() - right.eval();
			default:
				return left.eval();
		}
		
	}

}
