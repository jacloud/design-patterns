import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExpressionProcessor {
	
	public Map<Character, Integer> variables = new HashMap<>();
	
	
	public static enum expression {
		NUMBER,
		EXPRESSION
	}
	
	public expression expressionType;
	
	public List<Token> lex(String input) {
		
		List<Token> result = new ArrayList<>();
		char c;
		char[] chararray = input.toCharArray();
		StringBuilder sb;
		for(int i = 0; i < chararray.length; i++) {
			c = chararray[i];
			switch (c) {
				case '+':
					result.add(new Token(Token.typeExpresion.PLUS, "+"));
					break;
				case '-':
					result.add(new Token(Token.typeExpresion.MINUS, "-"));
					break;
				default:
					sb = new StringBuilder();
					sb.append(c);
					if(Character.isDigit(c)) {
						expressionType = expression.NUMBER;
					} else {
						expressionType = expression.EXPRESSION;
					}
					while(i+1 < chararray.length && checkConditionFinish(chararray[i+1], expressionType)) {
						c = chararray[i+1];
						sb.append(c);
						i++;
					}
					switch (expressionType) {
						case EXPRESSION:
							result.add(new Token(Token.typeExpresion.EXPRESION, sb.toString()));
							break;
						case NUMBER:
							result.add(new Token(Token.typeExpresion.INTEGER, sb.toString()));
							break;
						default:
							break;
					}

			}
		}
		return result;
	}
	
	public boolean checkConditionFinish(char c, expression expression) {
		
		switch (expression) {
			case EXPRESSION:
				return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
			case NUMBER:
				return (c >= '0' && c <= '9');
			default:
				return false;
		}

	}
	
	public Element parse(List<Token> tokens) {
		BinaryOperation bi = new BinaryOperation();
		boolean left = false;
		int value;
		Token token;
		for( int i = 0; !bi.fail && i < tokens.size(); i++) {
			
			token = tokens.get(i);
			
			switch (token.type) {
			
				case EXPRESION:
					if(token.text.length() > 1) {
						bi.fail = true;
						break;
					}
					else {
						Character character = new Character(token.text.charAt(0));
						if( variables.containsKey(character) ) {
							value = variables.get(character);
							if(!left) {
								bi.left = new Number(value);
								left = true;
							} else {
								bi.right = new Number(value);
								bi.left = new Number(bi.eval());
								bi.right = new Number(0);
							}
						} else {
							bi.fail = true;
						}
					}
					break;
				case INTEGER:
					value = Integer.parseInt(token.text);
					if(!left) {
						bi.left = new Number(value);
						left = true;
					} else {
						bi.right = new Number(value);
						bi.left = new Number(bi.eval());
						bi.right = new Number(0);
					}
					break;
				case MINUS:
					bi.type = BinaryOperation.typeOperation.SUBTRACTION;
					break;
				case PLUS:
					bi.type = BinaryOperation.typeOperation.ADDTION;
					break;
				default:
					break;

			}
		}
		return bi;
	}

	public int calculate(String expression) {
		if(expression.isEmpty()) return 0;
		List<Token> tokens = lex(expression);
		Element parse = parse(tokens);
		return parse.eval();
	  
	}
}
