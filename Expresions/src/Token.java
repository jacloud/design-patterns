
public class Token {
	
	public static enum typeExpresion {
		INTEGER,
		PLUS,
		MINUS,
		EXPRESION
	}
	
	public typeExpresion type;
	public String text;
	
	public Token(typeExpresion type, String text) {
		this.type = type;
		this.text = text;
	}
}
