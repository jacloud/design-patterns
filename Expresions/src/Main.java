
public class Main {

	public static void main(String[] args) {
		ExpressionProcessor expression = new ExpressionProcessor();
		System.out.println(expression.calculate("1"));
		
		System.out.println(expression.calculate("1+2"));
		
		expression.variables.put('x', 5);
		System.out.println(expression.calculate("1+x"));
		
		System.out.println(expression.calculate("1+xy"));

	}

}
