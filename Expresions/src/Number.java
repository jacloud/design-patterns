
public class Number implements Element {
	
	public int value;
	
	public Number(int value) {
		this.value = value;
	}

	@Override
	public int eval() {
		return value;
	}

}
