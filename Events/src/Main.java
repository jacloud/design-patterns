import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		Game game = new Game();
		
		Rat rat1 = new Rat(game);
		System.out.println(rat1.attack);
		
		Rat rat2 = new Rat(game);
		System.out.println(rat1.attack);
		System.out.println(rat2.attack);
		
		rat1.close();
		rat2.close();
	}

}
