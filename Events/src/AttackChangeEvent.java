
public class AttackChangeEvent {
	
	public Object source;
	public int attack;
	
	public AttackChangeEvent(Object o, int newAttack) {
		this.source = o;
		attack = newAttack;
	}
}
