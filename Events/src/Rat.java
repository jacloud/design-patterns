import java.io.Closeable;
import java.io.IOException;

public class Rat implements Closeable {
	
	private Game game;
	public int attack = 1;
	private String name;
	
	public Rat(Game game) {
		this.game = game;
		
		name = "I'm Rat " +this.game.list.size();
		
		System.out.println(name);
		
		game.attackChanged.addHandler(x -> {
			System.out.println(name + ": change attack");
			attack = x.attack;
		});
		
		game.list.add(this);
		
		System.out.println(name + ": increase attack");
		game.attackChanged.fire(new AttackChangeEvent(
				this, game.list.size() ));
	}

	@Override
	public void close() throws IOException {
		game.list.remove(this);
		System.out.println(name + ": decrease attack");
		game.attackChanged.fire(new AttackChangeEvent(
				this, game.list.size() ));
	}
}
