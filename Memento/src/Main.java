
public class Main {

	public static void main(String[] args) {
		TokenMachine tm = new TokenMachine();

		Memento memento = tm.addToken(111);
		
		tm.addToken(333);
		
		tm.revert(memento);
		
		for(Token t : tm.tokens) {
			System.out.println(t.value);
		}
		
	}

}
