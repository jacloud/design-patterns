import java.util.ArrayList;
import java.util.List;

public class TokenMachine {
	
	public List<Token> tokens = new ArrayList<>();

	public Memento addToken(int value)
	{
		tokens.add(new Token(value));
	    return new Memento(tokens);
	}

	public Memento addToken(Token token) {
	    tokens.add(token);
	    return new Memento(tokens);
	}

	public void revert(Memento m) {
	    tokens = m.tokens;
	}
}
