import java.util.ArrayList;
import java.util.List;

public class Memento {
	
	public List<Token> tokens = new ArrayList<>();
	  
	public Memento(List<Token> tokens) {
	    this.tokens = new ArrayList<>();
	    for(Token token : tokens) {
	    	this.tokens.add(new Token(token.value));
	    }
	}
}
