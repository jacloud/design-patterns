
public class PersonBuilder<SELF extends PersonBuilder<SELF>> {

	protected Person p;
	
	public PersonBuilder() {
		p = new Person();
	}
	
	public SELF self() {
		return (SELF) this;
	}
	
	public SELF withName(String name) {
		p.name = name;
		return self();
	}
	
	public Person build() {
		return p;
	}
}
