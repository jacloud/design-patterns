
public class Main {

	public static void main(String[] args) {
		PersonBuilder pb = new PersonBuilder();
		Person p = pb.withName("Jose").build();
		System.out.println(p.toString());
		
		EmployeeBuilder eb = new EmployeeBuilder();
		p = eb.withName("Antonio").worksAt("developer").build();
		System.out.println(p.toString());
		
		PersonBuilderFacade pbf = new PersonBuilderFacade();
		p = pbf.name().at("Jose").position().at("programmer").build();
		System.out.println(p.toString());
	}

}
