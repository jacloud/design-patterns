
public class Person {

	protected String name;
	protected String position;
	
	@Override
	public String toString() {
		return "name: " + name + " - position: " + position;
	}
}
