
public class PersonPositionBuilder extends PersonBuilderFacade {

	public PersonPositionBuilder(Person person) {
		this.p = person;
	}
		
	public PersonPositionBuilder at(String position) {
		p.position = position;
		return this;
	}
}
