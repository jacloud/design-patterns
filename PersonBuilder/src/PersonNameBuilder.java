
public class PersonNameBuilder extends PersonBuilderFacade {

	public PersonNameBuilder(Person person) {
		this.p = person;
	}
	
	public PersonNameBuilder at(String name) {
		p.name = name;
		return this;
	}
}
