
public class PersonBuilderFacade {
	
	protected Person p;
	
	public PersonBuilderFacade() {
		p = new Person();
	}
	
	public Person build() {
		return p;
	}
	
	public PersonNameBuilder name() {
		return new PersonNameBuilder(p);
	}
	
	public PersonPositionBuilder position() {
		return new PersonPositionBuilder(p);
	}

}
