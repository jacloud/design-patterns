public class EmployeeBuilder extends PersonBuilder<EmployeeBuilder> {
	
	public EmployeeBuilder worksAt(String position) {
		p.position = position;
		return self();
	}
	
	public EmployeeBuilder self() {
		return this;
	}
}
